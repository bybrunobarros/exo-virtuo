module.exports = {
  root: true,
  plugins: ["prettier", "security"],
  env: {
    jest: true,
    node: true
  },
  rules: {
    "prettier/prettier": "error"
  },
  extends: ["plugin:security/recommended"]
};
