const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

/**
 * @constructor Car
 */
const schema = new Schema(
  {
    name: {
      type: String,
      unique: true,
      maxlength: 3
    },
    station: { type: ObjectId, ref: "Station" },
    available: {
      type: Boolean,
      default: false
    }
  },
  { timestamps: true }
);

/**
 * @module Car
 */
module.exports = mongoose.model("Car", schema);
