const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

/**
 * @constructor Station
 */
const schema = new Schema(
  {
    name: {
      type: String,
      unique: true,
      maxlength: 3
    },
    cars: [{ type: ObjectId, ref: "Car" }]
  },
  { timestamps: true }
);

/**
 * @module Station
 */
module.exports = mongoose.model("Station", schema);
