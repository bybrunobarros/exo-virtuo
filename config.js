require("dotenv").config();
const env = process.env;

module.exports = {
  PORT: env.PORT || 8080,
  MONGO: {
    USER: env.MONGO_USER || "user",
    PASSWORD: env.MONGO_PASSWORD || "password",
    HOST: env.MONGO_HOST || "127.0.0.1",
    PORT: env.MONGO_PORT || "27017",
    NAME: env.MONGO_NAME || "virtuo"
  }
};
