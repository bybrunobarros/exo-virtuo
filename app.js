require("mongoose");
const express = require("express");
require("express-async-errors");
const bodyParser = require("body-parser");

const app = express();

app.use(bodyParser.json());

app.use("/", require("./api/route"));

app.use(function errorHandler(err, req, res, next) {
  if (err.joi && err.joi.isJoi) {
    return res.status(400).json({
      message: err.joi.message
    });
  } else {
    res.status(err.status).json({
      message: err.message
    });
  }
});

module.exports = app;
