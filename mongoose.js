const { MONGO } = require("./config");
const mongoose = require("mongoose");

mongoose.connect(
  `mongodb://${MONGO.USER}:${MONGO.PASSWORD}@${MONGO.HOST}:${MONGO.PORT}/${MONGO.NAME}`,
  {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false
  }
);

const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", () => console.log("Database running..."));
