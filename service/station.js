const Station = require("../model/Station");

function getAll() {
  return Station.find({});
}

async function create(name) {
  try {
    const station = new Station({ name });
    await station.validate();
    return station.save();
  } catch (e) {
    throw e;
  }
}

module.exports = {
  getAll,
  create
};
