# Virtuo: Software Engineer - Backend Task

The project is a small service which exposes two REST endpoints displaying stations and cars. 

## Installation
First of all, you should have [node.js](https://nodejs.org/en/) and npm or [yarn](https://yarnpkg.com/fr/docs/install) installed. 
npm is installed by default with node.js.

Then run one of these commands to install all dependencies.
```bash
npm install
# or
yarn install
```
## Before Usage
You might want to configure this app to fit your environment. To do this you can add an .env file at the root level of the project.
Here are what can be configured and the default values.

| Name  | Default |
| ------------- | ------------- |
| PORT  | 8080  |
| MONGO_USER  | user  |
| MONGO_PASSWORD  | password  |
| MONGO_HOST  | 127.0.0.1  |
| MONGO_PORT  | 27017  |
| MONGO_NAME  | virtuo  |

## Usage
To start the project run one of these commands.

```bash
npm run start
# or
yarn start
```

The project will start on http://localhost:8080 by default.



