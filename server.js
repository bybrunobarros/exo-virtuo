const { PORT } = require("./config");
require("./mongoose");
const app = require("./app");

app.listen(PORT);
console.log(`The server is running on http://localhost:${PORT}`);
