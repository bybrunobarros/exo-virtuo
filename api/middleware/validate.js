const { celebrate } = require("celebrate");

function validate(validation) {
  return (req, res, next) => {
    if (!validation) return next();
    return celebrate(validation)(req, res, next);
  };
}

module.exports = validate;
