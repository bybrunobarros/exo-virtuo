const stationService = require("../../service/station");

async function retrieveAll(req, res, next) {
  const stations = await stationService.getAll();
  res.json(stations);
}
async function retrieve(req, res, next) {
  res.json({});
}
async function create(req, res, next) {
  try {
    const station = await stationService.create(req.body.name);

    res.json({
      name: station.name,
      cars: [],
      createdAt: station.createdAt
    });
  } catch (e) {
    throw e;
  }
}
async function update(req, res, next) {
  res.json({});
}
async function remove(req, res, next) {
  res.json({});
}

module.exports = {
  retrieveAll,
  retrieve,
  create,
  update,
  remove
};
