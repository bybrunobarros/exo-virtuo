async function retrieveAll(req, res, next) {
  res.json([]);
}
async function retrieve(req, res, next) {
  res.json({});
}
async function create(req, res, next) {
  res.json({});
}
async function update(req, res, next) {
  res.json({});
}
async function remove(req, res, next) {
  res.json({});
}

module.exports = {
  retrieveAll,
  retrieve,
  create,
  update,
  remove
};
