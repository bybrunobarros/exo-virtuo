const express = require("express");
const { Joi } = require("celebrate");
const router = express.Router();

const middleware = require("./middleware");

const endpoints = {
  cars: {
    list: {
      endpoint: "/cars",
      method: "get",
      middleware: middleware.cars.retrieveAll
    },
    item: {
      endpoint: "/cars/:id",
      method: "get",
      middleware: middleware.cars.retrieve
    },
    create: {
      endpoint: "/cars",
      method: "post",
      middleware: middleware.cars.create
    },
    update: {
      endpoint: "/cars/:id",
      method: "put",
      middleware: middleware.cars.update
    },
    remove: {
      endpoint: "/cars/:id",
      method: "delete",
      middleware: middleware.cars.remove
    }
  },
  stations: {
    list: {
      endpoint: "/stations",
      method: "get",
      middleware: middleware.stations.retrieveAll
    },
    item: {
      endpoint: "/stations/:id",
      method: "get",
      middleware: middleware.stations.retrieve
    },
    create: {
      endpoint: "/stations",
      method: "post",
      validation: {
        body: {
          // SPEC: Allow two urls
          name: Joi.string()
            .min(3)
            .required()
        }
      },
      middleware: middleware.stations.create
    },
    update: {
      endpoint: "/stations/:id",
      method: "put",
      middleware: middleware.stations.update
    },
    remove: {
      endpoint: "/stations/:id",
      method: "delete",
      middleware: middleware.stations.remove
    }
  }
};

Object.entries(endpoints).forEach(endpointEntry => {
  Object.entries(endpointEntry[1]).forEach(endpoint => {
    const config = endpoint[1];
    router[config.method || "get"](
      config.endpoint,
      middleware.validate(config.validation),
      config.middleware
    );
  });
});

module.exports = router;
